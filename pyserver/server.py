from daemon import daemon
import sys
from re import sub
from epics import PV
from time import sleep
from random import randrange
import argparse

class MissingPVException(Exception):
    """ Raised if a PV that hasn't been initialized is requested. """

class pvdaemon(daemon):    
    def __init__(self, pidfile, username):
        self.requestb = False
        self.user = username
        self.pvs = {}
        self.reset()
        super(pvdaemon,self).__init__(pidfile)

    def run(self):
        """
        Implements the run() method from the super <daemon> class.
        """
        #PVs to listen to:
        self.addPv("request", monitor=True)
        self.addPv("downloadb", monitor=True)

        #Pvs to write to:
        self.addPv("replied")
        self.addPv("data")
        self.addPv("finishedb")

        while(True):
            while self.changedpvs: # Multiple PVs could be changed per second.
                pvname, value = self.changedpvs.pop()
                if pvname == self.fullPvName("request"):
                    if value:
                        print("Daemon: Received request")
                        self.getPv("replied").put(True)
                    else:
                        self.getPv("replied").put(False)
                elif pvname == self.fullPvName("downloadb") \
                    and value \
                    and self.getPv("request").get(): # We should probably also check that they have requested data instead of just requesting a download!

                    print("Daemon: download requested")
                    self.getPv("data").put(self.getData(100))
                    self.getPv("finishedb").put(True)
            sleep(1)

    def getPv(self, name):
        """
        Reads the PV from the dictionary.
        """
        pvname = self.fullPvName(name)
        try:
            pv = self.pvs[pvname]
        except KeyError:
            raise MissingPVException("Missing PV: %s" % pvname) from None
        return pv

    def addPv(self, name, monitor=False):
        """
        :name:      Which PV to link to.

        :monitor:  Should we monitor the PV?

        Creates a PV that the daemon will be able to read/write to/from.
        """
        pvname = self.fullPvName(name)
        self.pvs[pvname] = PV(pvname, callback=self.reply, auto_monitor=monitor)

    def reply(self, **kw):
        """
        Generic callback function for PV. Reads in value and PV name that was changed for processing later.
        """
        try:
            self.setPvValue(kw["pvname"], kw["value"])
        except KeyError:
            pass

    def setPvValue(self, pvname, value):
        self.changedpvs.append((pvname, value))

    def reset(self):
        self.changedpvs = []

    def fullPvName(self, name):
        return self.user + ":" + name

    def getData(self, count):
        """
        Dummy function to return ``count`` worth of data.
        """
        data = [0] * count
        for i in range(count):
            data[i] = (randrange(2*i//3,i+1) ** 2)/10000.0
        return data

def cleanupUserName(name):
    return sub('[^a-zA-Z0-9_]','_',name)

def main(argv):
    """
    Runs the python daemon with the correct arguments
    """
    parser = argparse.ArgumentParser(description="Test python daemon to communicate with calibration client")

    parser.add_argument("action",choices=["start", "stop"])
    parser.add_argument("macro",help="Macro to define which PVs to look for")

    args = parser.parse_args()

    pidname = "/tmp/dex-%s.pid" % cleanupUserName(args.macro)
    pvd = pvdaemon(pidname, args.macro)
    if args.action == "start":
        pvd.start()
    else:
        pvd.stop()


if __name__=="__main__":
    main(sys.argv[1:])